﻿using UnityEngine;
using System.Collections;

public interface SceneData{
	string getType ();
	string getName ();
}
